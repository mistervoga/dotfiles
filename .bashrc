#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
 exec tmux
fi

export FZF_DEFAULT_COMMAND='rg --files --hidden -g "!.git" ' 

function cd_up() {
  cd $(printf "%0.s../" $(seq 1 $1 ));
}

# get current status of git repo
function parse_git_dirty {
    status=$(git status --porcelain 2>/dev/null)
    if [[ -n "$status" ]]; then
        echo "!"
    else
        echo ""
    fi
}

# get current branch in git repo
function parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=`parse_git_dirty`
		echo "[${BRANCH}${STAT}]"
	else
		echo ""
	fi
}



#ui customizations
export PS1="\[\e[01;31m\][\[\e[m\]\[\e[01;31m\]\u\[\e[m\]\[\e[01;31m\]@\[\e[m\]\[\e[01;31m\]\h\[\e[m\]:\[\e[01;36m\]\w\[\e[m\] \[\e[33m\]\`parse_git_branch\`\[\e[m\]\[\e[01;31m\]]\[\e[m\]\[\e[01;31m\]\\$\[\e[m\] "


alias pin="sudo pacman -S"
alias pup="sudo pacman -Syu"
alias prem="sudo pacman -Rns"
alias open=xdg-open
alias ls='ls --color=auto'
alias scan='scanimage --format=png --output-file file.png --progress'
alias ..="cd_up"
alias copy="xclip -selection c"

alias record='ffmpeg -f alsa -i default -t 00:05:00 recording.wav'


#path declarations

export PATH="$HOME/bin:$PATH"
 
export PATH="$HOME/.local/bin:$PATH"
export PATH="$PATH/usr/bin/elixir"
export PATH="$PATH/usr/bin/mix"
export PATH="$HOME/.rebar3:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.bun/bin:$PATH"
export PATH="$HOME/.zig:$PATH"

export GOROOT="$HOME/bin/go"
export GOPATH="$HOME/bin/go/pkg"
export PATH="$PATH:$GOROOT/bin"
export PATH="/opt/android-sdk:$PATH"

export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
export PATH="$HOME/.pyenv/bin:$PATH"
eval "$(pyenv init -)"



